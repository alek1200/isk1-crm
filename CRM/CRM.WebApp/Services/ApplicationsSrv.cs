﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Appications;
using CRM.WebApp.Models.Appications.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Services
{
    /// <summary>
    /// This class is manages applications.
    /// </summary>
    public class ApplicationSrv
    {
        #region  ========== Fields ==========

        /// <summary>
        /// Database Access Object
        /// </summary>
        private readonly CrmDbContext _database;

        #endregion

        /// <summary>
        /// Constructor.
        /// </summary>
        public ApplicationSrv()
        {
            _database = new CrmDbContext();
        }


        #region ========== Methods ==========

        public Complaint CreateComplaint()
        {
            throw new NotImplementedException();
        }

        public ServiceRequest CreateServiceRequest()
        {
            ServiceRequest service = new ServiceRequest();



            return service;
        }
        
        #endregion
    }
}