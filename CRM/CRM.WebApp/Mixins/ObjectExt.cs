﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Mixins
{
    public static class ObjectExt
    {
        /// <summary>
        /// Checks objects existance.
        /// </summary>
        /// <param name="target">Compared object.</param>
        /// <returns>Returns true if reference to an object is set to null.</returns>
        public static bool IsNull(this object target)
        {
            return target == null;
        }
    }
}