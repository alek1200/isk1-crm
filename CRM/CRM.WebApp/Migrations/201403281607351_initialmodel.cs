namespace CRM.WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialmodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Community = c.String(nullable: false, maxLength: 100),
                        PostalCode = c.String(nullable: false, maxLength: 5),
                        City = c.String(nullable: false, maxLength: 100),
                        BuildingNr = c.Int(nullable: false),
                        ApartmentNr = c.Int(nullable: false),
                        District = c.String(nullable: false, maxLength: 100),
                        Street = c.String(nullable: false, maxLength: 100),
                        Province = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AddressId);
            
            CreateTable(
                "dbo.Analyzes",
                c => new
                    {
                        AnalyzeId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.AnalyzeId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        CompanyID = c.Int(),
                        CompanyName = c.String(maxLength: 40),
                        NIP = c.Int(),
                        PersonId = c.Int(),
                        Email = c.String(maxLength: 100),
                        Name = c.String(maxLength: 100),
                        Surname = c.String(maxLength: 100),
                        PhoneNr = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        ClientAddress_AddressId = c.Int(),
                    })
                .PrimaryKey(t => t.ClientId)
                .ForeignKey("dbo.Addresses", t => t.ClientAddress_AddressId)
                .Index(t => t.ClientAddress_AddressId);
            
            CreateTable(
                "dbo.Complaints",
                c => new
                    {
                        ComplaintId = c.Int(nullable: false, identity: true),
                        CreationTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        Description = c.String(nullable: false, maxLength: 100),
                        ComplaintStatus = c.String(),
                        Trace_TraceId = c.Int(),
                        Client_ClientId = c.Int(),
                    })
                .PrimaryKey(t => t.ComplaintId)
                .ForeignKey("dbo.Traces", t => t.Trace_TraceId)
                .ForeignKey("dbo.Clients", t => t.Client_ClientId)
                .Index(t => t.Trace_TraceId)
                .Index(t => t.Client_ClientId);
            
            CreateTable(
                "dbo.Traces",
                c => new
                    {
                        TraceId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.TraceId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Login = c.String(maxLength: 100),
                        Password = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.ServiceRequests",
                c => new
                    {
                        ServiceRequestId = c.Int(nullable: false, identity: true),
                        CreationTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        Analyze_AnalyzeId = c.Int(),
                        Maintance_MaintenanceId = c.Int(),
                        Trace_TraceId = c.Int(),
                        Client_ClientId = c.Int(),
                    })
                .PrimaryKey(t => t.ServiceRequestId)
                .ForeignKey("dbo.Analyzes", t => t.Analyze_AnalyzeId)
                .ForeignKey("dbo.Maintenances", t => t.Maintance_MaintenanceId)
                .ForeignKey("dbo.Traces", t => t.Trace_TraceId)
                .ForeignKey("dbo.Clients", t => t.Client_ClientId)
                .Index(t => t.Analyze_AnalyzeId)
                .Index(t => t.Maintance_MaintenanceId)
                .Index(t => t.Trace_TraceId)
                .Index(t => t.Client_ClientId);
            
            CreateTable(
                "dbo.Maintenances",
                c => new
                    {
                        MaintenanceId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.MaintenanceId);
            
            CreateTable(
                "dbo.Repairs",
                c => new
                    {
                        RepairId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.RepairId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceRequests", "Client_ClientId", "dbo.Clients");
            DropForeignKey("dbo.ServiceRequests", "Trace_TraceId", "dbo.Traces");
            DropForeignKey("dbo.ServiceRequests", "Maintance_MaintenanceId", "dbo.Maintenances");
            DropForeignKey("dbo.ServiceRequests", "Analyze_AnalyzeId", "dbo.Analyzes");
            DropForeignKey("dbo.Complaints", "Client_ClientId", "dbo.Clients");
            DropForeignKey("dbo.Complaints", "Trace_TraceId", "dbo.Traces");
            DropForeignKey("dbo.Traces", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Clients", "ClientAddress_AddressId", "dbo.Addresses");
            DropIndex("dbo.ServiceRequests", new[] { "Client_ClientId" });
            DropIndex("dbo.ServiceRequests", new[] { "Trace_TraceId" });
            DropIndex("dbo.ServiceRequests", new[] { "Maintance_MaintenanceId" });
            DropIndex("dbo.ServiceRequests", new[] { "Analyze_AnalyzeId" });
            DropIndex("dbo.Complaints", new[] { "Client_ClientId" });
            DropIndex("dbo.Complaints", new[] { "Trace_TraceId" });
            DropIndex("dbo.Traces", new[] { "User_UserId" });
            DropIndex("dbo.Clients", new[] { "ClientAddress_AddressId" });
            DropTable("dbo.Repairs");
            DropTable("dbo.Maintenances");
            DropTable("dbo.ServiceRequests");
            DropTable("dbo.Users");
            DropTable("dbo.Traces");
            DropTable("dbo.Complaints");
            DropTable("dbo.Clients");
            DropTable("dbo.Analyzes");
            DropTable("dbo.Addresses");
        }
    }
}
