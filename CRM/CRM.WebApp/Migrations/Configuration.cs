namespace CRM.WebApp.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CRM.WebApp.Models;

    using CRM.WebApp.Models.Appications;
    using CRM.WebApp.Models.Appications.Services;
    internal sealed class Configuration : DbMigrationsConfiguration<CRM.WebApp.Models.CrmDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "CrmDbContext";
        }

        protected override void Seed(CRM.WebApp.Models.CrmDbContext context)
        {
            var Users = new List<User>
            {
                new User {Login = "test1", Password = "test1"},
                new User {Login = "test2", Password = "test2"}
            };
            Users.ForEach(s => context.Users.AddOrUpdate(p => p.UserId, s));

            // Add sample service requests
            var serviceRequets = SeedsGenerator.GenerateSampleServiceRequests();
            foreach (var request in serviceRequets)
                context.ServiceRequests.Add(request);

            context.SaveChanges();
        }
    }
}
