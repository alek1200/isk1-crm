﻿using CRM.WebApp.Models.Appications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Migrations
{
    public class SeedsGenerator
    {
        /// <summary>
        /// Generates 3 predifined sample service requests.
        /// </summary>
        /// <returns>ServiceRequests collection.</returns>
        public static IEnumerable<ServiceRequest> GenerateSampleServiceRequests()
        {
            #region request #0
            ServiceRequest r0 = new ServiceRequest()
            {
                Title = "Komputer się nie włącza",
                ClientInterview = "Po włączeniu komputer wydaje dziwne dźwięki i nie startuje. Cały czas czarny ekran.",
                CreationTime = DateTime.Now,
                Maintenance = new Models.Appications.Services.Maintenance() { Description = "Wyczyszczenie procesora z kurzu. Nałożenie pasty na procesor." },
                PredictedRealizationDate = new DateTime(DateTime.Now.Ticks + 1000 * 60 * 60 * 24 * 14),
                Repair = new Models.Appications.Services.Repair() { Description = "Naprawiono ścieżki na płycie głównej. Bla bla bla...." },
                Status = ServiceRequestStatus.Completed
            };
            #endregion

            #region request #1
            ServiceRequest r1 = new ServiceRequest()
            {
                Title = "Napęd nie chce odtwarzać płyt.",
                ClientInterview = "Napęd nie odtwarza żadnych płyt. Sprawdzano z CD i DVD.",
                CreationTime = new DateTime(2008, 11, 23, 11, 22, 33),
                PredictedRealizationDate = new DateTime(new DateTime(2008, 11, 23, 11, 22, 33).Ticks + 1000 * 60 * 60 * 24 * 14),
                Status = ServiceRequestStatus.Await
            };
            #endregion

            #region request #2
            ServiceRequest r2 = new ServiceRequest()
            {
                Title = "Drukarka nie chce drukować.",
                ClientInterview = "Przy próbie druku zapala się zółta lampka i hakuna matata.",
                CreationTime = new DateTime(2000, 1, 1, 3, 2, 13),
                PredictedRealizationDate = new DateTime(new DateTime(2000, 1, 1, 3, 2, 13).Ticks + 1000 * 60 * 60 * 24 * 10),
                Status = ServiceRequestStatus.Await
            };
            #endregion

            IEnumerable<ServiceRequest> requests = new List<ServiceRequest>()
            {
                r0, r1, r2
            };

            return requests;
        }
    }
}