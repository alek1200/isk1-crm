﻿using System.Web;
using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;

namespace CRM.WebApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Variables
            bundles.UseCdn = true;
            var nullBuilder = new NullBuilder();
            var cssTransformer = new CssTransformer();
            var jsTransformer = new JsTransformer();
            var nullOrderer = new NullOrderer();

            // JavaScript

            // jQuery
            var jquery = new Bundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js");
            jquery.Builder = nullBuilder;
            jquery.Transforms.Add(jsTransformer);
            jquery.Orderer = nullOrderer;

            bundles.Add(jquery);

            // Modernizr
            var modernizr = new Bundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-2.6.2.js");
            modernizr.Builder = nullBuilder;
            modernizr.Transforms.Add(jsTransformer);
            modernizr.Orderer = nullOrderer;

            bundles.Add(modernizr);

            // Scripts
            var js = new Bundle("~/bundles/js").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/myScripts.js");
            js.Builder = nullBuilder;
            js.Transforms.Add(jsTransformer);
            js.Orderer = nullOrderer;
            bundles.Add(js);

            // CoffeeScripts
            var coffee = new Bundle("~/bundles/coffee")
                .Include("~/Scripts/coffee/CommonScripts.coffee");
            coffee.Builder = nullBuilder;
            coffee.Transforms.Add(jsTransformer);
            coffee.Orderer = nullOrderer;
            bundles.Add(coffee);


            // POST: Validation
            var jqueryVal = new Bundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/jquery.validate.min.js");
            jqueryVal.Builder = nullBuilder;
            jqueryVal.Transforms.Add(jsTransformer);
            jqueryVal.Orderer = nullOrderer;

            bundles.Add(jqueryVal);

            // Styles
            var css = new Bundle("~/bundles/css");
            css.Include("~/Content/bootstrap/bootstrap.less");
            css.IncludeDirectory("~/Content/less", "*.less");
            css.Builder = nullBuilder;
            css.Transforms.Add(cssTransformer);
            css.Transforms.Add(new CssMinify());
            css.Orderer = nullOrderer;

            bundles.Add(css);

            BundleTable.EnableOptimizations = true;

            #region Commented out to be removed in the future if not necessery.

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //            "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Content/themes/base/jquery.ui.button.css",
            //            "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
            //            "~/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Content/themes/base/jquery.ui.theme.css"));

            #endregion
        }
    }
}