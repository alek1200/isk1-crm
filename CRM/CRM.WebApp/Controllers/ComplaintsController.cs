﻿using CRM.WebApp.Models.Appications;
using CRM.WebApp.Mixins;
using CRM.WebApp.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace CRM.WebApp.Controllers
{
    public class ComplaintsController : Controller
    {
        /// <summary>
        /// Data access object
        /// </summary>
        public CrmDbContext _db;

        /// <summary>
        /// Constructor
        /// </summary>
        public ComplaintsController()
        {
            // Initilize variables 

            if (_db == null)
            {
                _db = new CrmDbContext();
            }
        }

        //
        // GET: /Complaints/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //
        // GET: /Complaints/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Complaints/Create
        [HttpPost]
        public ActionResult Create(Complaint complaint)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Set complaints creation time to this moment.
                    complaint.CreationTime = DateTime.Now;

                    _db.Complaints.Add(complaint);
                    _db.SaveChanges();
                }

                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Complaints/List
        public ActionResult List()
        {
            if (_db.IsNull())
                throw new NullReferenceException("Database context is null!");

            IEnumerable<Complaint> complaints = _db.Complaints;

            return View(complaints);
        }

        //
        // GET: /Complaints/Edit/5
        public ActionResult Edit(int id)
        {
            Complaint complaint = null;

            try
            {
                complaint = _db.Complaints.First(r => r.ComplaintId == id);
            }
            catch (InvalidOperationException e)
            {
                // TODO add redirect
                throw e;
            }

            return View(complaint);
        }

        //
        // POST: /Complaints/Edit
        [HttpPost]
        public ActionResult Edit(Complaint complaint)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(complaint).State = EntityState.Modified;
                    _db.SaveChanges();

                    // Redirect to service request details view
                    return RedirectToAction("Details", new { id = complaint.ComplaintId });
                }
            }
            catch (Exception ex)
            {
                // TODO add error msg
                return View();
            }

            // If received request is not valid, show edit view again for necessery corrections.
            return View(complaint);
        }

        //
        // GET: /Complaints/Details/5
        public ActionResult Details(int id)
        {
            Complaint complaint = null;

            try
            {
                complaint = _db.Complaints.First(c => c.ComplaintId == id);
            }
            catch (InvalidOperationException e)
            {
                // TODO add redirect
                throw e;
            }

            return View(complaint);
        }

        public ActionResult Delete(int id)
        {
            if (_db.IsNull())
                throw new NullReferenceException("Database context is null!");

            // Find complaint i db
            Complaint complaint = _db.Complaints.First(c => c.ComplaintId == id);

            if (complaint.IsNull())
            {
                // TODO add error for not finding complaint
            }

            // Remove complaint
            _db.Complaints.Remove(complaint);

            // Zsynchronizuj z db
            _db.SaveChanges();

            return RedirectToAction("List");
        }
    }
}