﻿using CRM.WebApp.Models.Appications;
using CRM.WebApp.Mixins;
using CRM.WebApp.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace CRM.WebApp.Controllers
{
    public class ServiceRequestsController : Controller
    {
        /// <summary>
        /// Data access object
        /// </summary>
        public CrmDbContext _db;

        /// <summary>
        /// Constructor
        /// </summary>
        public ServiceRequestsController()
        {
            // Initilize variables 

            if (_db == null)
            {
                _db = new CrmDbContext();
            }
        }

        //
        // GET: /ServiceRequests/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //
        // GET: /ServiceRequests/Details/5
        public ActionResult Details(int id)
        {
            ServiceRequest request = null;

            try
            {
                request = _db.ServiceRequests.First(r => r.ServiceRequestId == id);
            }
            catch (InvalidOperationException e)
            {
                // TODO add redirect
                throw e;
            }

            return View(request);
        }

        // GET: /ServiceRequests/List
        public ActionResult List()
        {
            IEnumerable<ServiceRequest> requests = _db.ServiceRequests;

            return View(requests);
        }

        //
        // GET: /ServiceRequests/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ServiceRequests/Create
        [HttpPost]
        public ActionResult Create(ServiceRequest request)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Set requests creation time to this moment.
                    request.CreationTime = DateTime.Now;

                    _db.ServiceRequests.Add(request);
                    _db.SaveChanges();
                }

                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ServiceRequests/Edit/5
        public ActionResult Edit(int id)
        {
            ServiceRequest request = null;

            try
            {
                request = _db.ServiceRequests.First(r => r.ServiceRequestId == id);
            }
            catch (InvalidOperationException e)
            {
                // TODO add redirect
                throw e;
            }

            return View(request);
        }

        //
        // POST: /ServiceRequests/Edit
        [HttpPost]
        public ActionResult Edit(ServiceRequest request)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Set all entries as modified so the changes shall affect.
                    object[] modifiedEntries = { request, request.Analyze, request.Maintenance, request.Recovery, request.Repair, request.Replacement };
                    foreach (object entry in modifiedEntries)
                    {
                        _db.Entry(entry).State = EntityState.Modified;
                    }
                    _db.SaveChanges();

                    // Redirect to service request details view
                    return RedirectToAction("Details", new { id = request.ServiceRequestId });
                }
            }
            catch (Exception ex)
            {
                // TODO add error msg
                return View();
            }

            // If received request is not valid, show edit view again for necessery corrections.
            return View(request);
        }

        //
        // GET: /ServiceRequests/Delete/5
        public ActionResult Delete(int id)
        {
            if (_db.IsNull())
                throw new NullReferenceException("Database context is null.");

            var request = _db.ServiceRequests.First(r => r.ServiceRequestId == id);
            if (request != null)
            {
                _db.ServiceRequests.Remove(request);
                _db.SaveChanges();
            }

            return RedirectToAction("List");
        }
    }
}
