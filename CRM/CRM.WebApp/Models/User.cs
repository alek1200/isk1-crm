﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.WebApp.Models
{
    public class User
    {
        public int UserId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        public string Login { get; set; }

        [StringLength(100, MinimumLength = 3)]
        public string Password { get; set; }
    }
}