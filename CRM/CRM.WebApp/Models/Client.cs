﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CRM.WebApp.Models.Appications;
using CRM.WebApp.Models.Appications.Services;
using CRM.WebApp.Models.Clients;

namespace CRM.WebApp.Models
{
    public class Client
    {
        public int ClientId { get; set; }

        public virtual Address ClientAddress { get; set; }

        public virtual ICollection<ServiceRequest> ServiceRequests { get; set; }

        public virtual ICollection<Complaint> Complaints { get; set; }

    }
}