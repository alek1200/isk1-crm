﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.WebApp.Models.Clients
{
    public class Address
    {
        public int AddressId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required(ErrorMessage = "Commune Name is required")]
        public string Community { get; set; } //gmina - jak ktoś ma lepsze tłumaczenie to niech da znać

        [StringLength(5)]
        [Required(ErrorMessage = "Postal Code Name is required")]
        public string PostalCode { get; set; }

        [StringLength(100, MinimumLength = 2)]
        [Required(ErrorMessage = "City Name is required")]
        public string City { get; set; }

     
        public int BuildingNr { get; set; } //nr budynku
        public int ApartmentNr { get; set; } //nr mieszkania

        [StringLength(100, MinimumLength = 3)]
        [Required(ErrorMessage = "District Name is required")]
        public string District { get; set; } //powiat

        [StringLength(100, MinimumLength = 3)]
        [Required(ErrorMessage = "Street Name is required")]
        public string Street{ get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string Province { get; set; } //województwo
    }
}