﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.WebApp.Models
{
    public class Company :Client
    {
        public int CompanyID { get; set; }

        [StringLength(40, MinimumLength = 3), Required]
        public string CompanyName { get; set; }

        [Range(10,10), Required]
        public int NIP { get; set; }
    }
}