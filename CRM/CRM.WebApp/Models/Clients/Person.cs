﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.WebApp.Models
{
    public class Person : Client
    {
        public int PersonId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string Email { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string Name { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string Surname { get; set; }
        public int PhoneNr { get; set; }
    }
}