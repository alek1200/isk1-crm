﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

using CRM.WebApp.Models.Appications;
using CRM.WebApp.Models.Clients;
using CRM.WebApp.Models.Appications.Services;

namespace CRM.WebApp.Models
{
    public class CrmDbContext : DbContext
    {
        public CrmDbContext()
            : base("CrmDbContext")
        {

        }

        // TODO opisać zbiory

        public DbSet<Client> Clients { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Complaint> Complaints { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<ServiceRequest> ServiceRequests { get; set; }
        public DbSet<Trace> Traces { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Repair> Repairs { get; set; }
        public DbSet<Analyze> Analysis { get; set; }
        public DbSet<Maintenance> Maintanence { get; set; }
    }
}