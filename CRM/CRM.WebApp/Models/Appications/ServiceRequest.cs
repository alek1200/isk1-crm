﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.WebApp.Models.Appications.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.WebApp.Models.Appications
{
    public class ServiceRequest
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        [DisplayName("Id")]
        public int ServiceRequestId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [Required]
        [StringLength(200, MinimumLength = 5)]
        [DisplayName("Tytuł zgłoszenia")]
        public string Title { get; set; }

        /// <summary>
        /// Trace to the user.
        /// </summary>
        public virtual Trace Trace { get; set; }

        /// <summary>
        /// Time when application been officialy started.
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// Time when application been officialy ended.
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Date until requested service task should be done.
        /// </summary>
        [Required]
        [DisplayName("Przewidywana data realizacji")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? PredictedRealizationDate { get; set; }

        [StringLength(1000)]
        [DisplayName("Wywiad z klientem")]
        public string ClientInterview { get; set; }

        /// <summary>
        /// Converted to enum ServiceRequestStatus. This property should be used in logic layer. EF6 sucks in some ways.
        /// If ServiceRequestStatus is null, returns default value = "Await".
        /// </summary>
        [NotMapped]
        [DisplayName("Status")]
        public ServiceRequestStatus Status
        {
            get { return (ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), !string.IsNullOrEmpty(ServiceRequestStatus) ? ServiceRequestStatus : CRM.WebApp.Models.Appications.ServiceRequestStatus.Await.ToString()); }
            set { ServiceRequestStatus = value.ToString(); }
        }

        /// <summary>
        /// Used only for mapping. Converted to Status property;
        /// </summary>
        public string ServiceRequestStatus { get; set; } // Must be public!


        // TODO wprowadzić flagi wskazujące na wybrane usługi

        public virtual Analyze Analyze { get; set; }
        public virtual Maintenance Maintenance { get; set; }
        public virtual Repair Repair { get; set; }
        public virtual Replacement Replacement { get; set; }
        public virtual Recovery Recovery { get; set; }
    }

    public enum ServiceRequestStatus
    {
        Await = 0, InProgress = 1, Completed = 2
    }
}