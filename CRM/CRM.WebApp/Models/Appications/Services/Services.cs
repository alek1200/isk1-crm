﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;


namespace CRM.WebApp.Models.Appications.Services
{
    // TODO jak się rozrośnie to przenieść poszczególne klasy do oddzielnych plików

    public class Analyze
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public Analyze()
        {

        }

        /// <summary>
        /// Id
        /// </summary>
        public int AnalyzeId { get; set; }

        /// <summary>
        /// Comment left after analysing the issue.
        /// </summary>
        [DisplayName("Opis analizy")]
        public string Description { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Maintenance
    {
        public int MaintenanceId { get; set; }

        [DisplayName("Opis konserwacji")]
        public string Description { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Replacement
    {
        public int ReplacementId { get; set; }

        [DisplayName("Opis wymiany")]
        public string Description { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Repair
    {
        /// <summary>
        /// Id
        /// </summary>
        public int RepairId { get; set; }

        [DisplayName("Opis naprawy")]
        public string Description { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class Recovery
    {
        /// <summary>
        /// Id
        /// </summary>
        public int RecoveryId { get; set; }

        [DisplayName("Opis odzyskiwania")]
        public string Description { get; set; }
    }
}